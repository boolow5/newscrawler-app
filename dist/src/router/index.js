import VueRouter from 'vue-router'

import {isLoggedIn} from '../store/modules/user'

import Home from '../components/Home.vue'
import CategoryDetail from '../components/CategoryDetail.vue'
import Settings from '../components/Settings.vue'
import About from '../components/About.vue'

const router = new VueRouter({
  // mode: 'history',
  base: __dirname,
  routes: [
    { name: 'Home', path: '*', component: Home, meta: {title: 'Home', authRequired: false} },
    { name: 'CategoryDetail', path: '/category/:category', component: CategoryDetail, meta: {title: 'Category Details', authRequired: false} },
    { name: 'Settings', path: '/settings', component: Settings, meta: {title: 'Settings', authRequired: false} },
    { name: 'About', path: '/about', component: About, meta: {title: 'About', authRequired: false} }
  ]
})

router.beforeEach((to, from, next) => {
  // console.log('beforeEach', {to, from, next})
  to.matched.some(route => {
    document.title = route.meta.title + ' | iWeydi News'
    if (route.meta.authRequired && !isLoggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  })
})

export default router
