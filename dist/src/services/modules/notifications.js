export default {
  isAvailable () {
    return window.cordova && window.cordova.plugins && window.cordova.plugins.notification && window.cordova.plugins.notification.local
  },
  isAllowed () {
    return (window.Notification && window.Notification.permission === 'granted') || localStorage.getItem('notification_premission') === 'granted'
  },
  initEvents () {
    console.log('initEvents', {available: this.isAvailable()})
    if (this.isAvailable()) {
      let vm = this
      window.cordova.plugins.notification.local.on('chat', function (notification, eopts) {
        console.log('chatNotificationEvent', {notification, eopts})
      })
      window.cordova.plugins.notification.local.on('click', function (notification, eopts) {
        console.log('local.Click', {notification, eopts})
        vm.clearMsg(notification.id)
      })
    }
  },
  clearMsg (id) {
    console.log('clearMsg', {id})
    if (this.isAvailable()) {
      window.cordova.plugins.notification.local.core.fireEvent('clear', {id: id})
    }
  },
  lastID: 0,
  getNextID () {
    this.lastID += 1
    return this.lastID
  },
  send (data) {
    let msgs = []
    // if data is an array send each object as different message
    if (data && typeof data === 'object' && data.constructor.name === 'Array') {
      for (let i = 0; i < data.length; i++) {
        if (data[i] && data[i].title && data[i].text) {
          data.id = this.getNextID()
          if (data.path && data.path.length > 0) {
            data.actions = [{id: 'chat', title: 'Reply'}]
          }
          msgs.push(data)
        }
      }
    } else if (data && typeof data === 'object' && data.constructor.name === 'Object' && data.title && data.text) { // not an array send as a single message
      data.id = this.getNextID()
      msgs.push(data)
    }
    if (msgs && msgs.length > 0) {
      if (this.isAvailable() && this.isAllowed()) {
        console.log('showing messages', msgs)
        window.cordova.plugins.notification.local.schedule(msgs)
      } else { // browser desktop notification instead
        console.log('WARNING: using desktop notification instead', window.Notification)
        if (window.Notification) {
          // if (Notification.permission !== 'granted') {
          //   Notification.requestPermission()
          // } else {}
          if (this.isAllowed()) {
            let url = location.origin.indexOf('file://') > -1 ? 'https://news.iweydi.com' : location.origin
            for (var i = 0; i < msgs.length; i++) {
              console.log(`i=${i}, msgs[${i}]:`, msgs[i])
              let notification = new Notification(msgs[i].title || 'https://news.iweydi.com', {
                icon: 'https://news.iweydi.com/dist/img/icons/favicon-32x32.png',
                body: `${msgs[i].text || 'New notification received!'}`
              })

              let index = i
              notification.onclick = function () {
                console.log(`notification[${i}].click`, msgs[index])
                if (msgs[index] && msgs[index].hasOwnProperty('path') > -1) {
                  msgs[index].path = msgs[index].path.indexOf('/') === 0 ? msgs[index].path : `/${msgs[index].path}`
                  url += '#' + msgs[index].path
                }
                console.log('Notification URL:', {msg: msgs[index], url})
                window.location.replace(url)
                notification.close()
              }
            }
          }
        }
      }
    }
  }
}
