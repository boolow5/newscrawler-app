const state = {
  categories: [],
  newsItems: []
}
const getters = {
  allItems: (state) => state.newsItems,
  allCategories: (state) => state.categories
}
const mutations = {
  SET_NEWS_CATEGORIES (state, payload) {
    state.categories = payload
  },
  SET_LATEST_NEWS_ITEMS (state, payload) {
    state.newsItems = payload
  }
}
const actions = {
  GET_NEWS_CATEGORIES ({commit, dispatch}, payload) {
    // console.log('GET_NEWS_CATEGORIES action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/news/categories').then(response => {
        // console.log('GET_NEWS_CATEGORIES response', response)
        if (response && response.data && response.data.categories) {
          commit('SET_NEWS_CATEGORIES', response.data.categories)
        }
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        // console.log('GET_NEWS_CATEGORIES error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  GET_LATEST_NEWS_ITEMS ({commit, dispatch}, payload) {
    // console.log('GET_LATEST_NEWS_ITEMS action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/news/latest').then(response => {
        // console.log('GET_LATEST_NEWS_ITEMS response', response)
        if (response && response.data && response.data.items) {
          commit('SET_LATEST_NEWS_ITEMS', response.data.items)
        }
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        // console.log('GET_LATEST_NEWS_ITEMS error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  GET_NEWS_BY_CATEGORY ({dispatch, commit}, category) {
    // console.log('GET_NEWS_CATEGORIES action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/news/latest?limit=30&category=' + category).then(response => {
        // console.log('GET_NEWS_CATEGORIES response', response)
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        // console.log('GET_NEWS_CATEGORIES error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  GET_NEWS_BY_SEARCH ({dispatch, commit}, payload) {
    // console.log('GET_NEWS_BY_SEARCH action')
    return new Promise((resolve, reject) => {
      let query = '?'
      if (payload) {
        query += (payload.category) ? `category=${payload.category}&` : ''
        query += (payload.q) ? `q=${payload.q}` : ''
      }
      window.VueInstance.$http.get('/api/v1/news/search' + query).then(response => {
        // console.log('GET_NEWS_BY_SEARCH response', response)
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        // console.log('GET_NEWS_BY_SEARCH error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
