export const isLoggedIn = function () {
  // console.log('isLoggedIn', {length: state.jwt_token.length, str: state.jwt_token, isTrue: state && state.jwt_token && state.jwt_token.length > 50})
  return state && state.jwt_token && state.jwt_token.length > 50
}

const state = {
  jwt_token: '',
  level: null,
  profile: {},
  role: null
}

const getters = {
  is_logged_in: (state) => state.jwt_token && state.jwt_token.length > 50,
  profile: (state) => state.profile
}

const mutations = {
  SET_TOKEN (state, token) {
    // console.log('SET_TOKEN', token)
    state.jwt_token = token
    localStorage.setItem('auth', JSON.stringify(state))
  },
  SET_LEVEL (state, level) {
    state.level = level
  },
  SET_USER_LEVEL (state, level) {
    state.role = level
  },
  SET_PROFILE (state, profile) {
    state.profile = profile
  },
  CLEAR_USER_DATA (state) {
    state.jwt_token = ''
    state.level = null
    state.profile = {}
  }
}

const actions = {
  USER_LEVEL ({commit, dispatch}) {
    // console.log('USER LEVEL action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/user-level').then(response => {
        // console.log('USER LEVEL response', response)
        if (response && response.data && response.data.token) {
          commit('SET_USER_LEVEL', response.data.level)
        }
        resolve(response.data)
      }).catch(err => {
        // console.log('USER LEVEL error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  LOGIN ({commit, dispatch}, data) {
    // console.log('LOGIN action', data)
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.post('/api/v1/login', data).then(response => {
        // console.log('LOGIN response', data.token)
        if (response && response.data && response.data.token) {
          commit('SET_TOKEN', response.data.token)
          commit('SET_LEVEL', response.data.level)
        }
        resolve(response.data)
      }).catch(err => {
        // console.log('LOGIN error', data)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  SIGNUP ({commit, dispatch}, data) {
    // console.log('SIGNUP action', data)
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.post('/api/v1/signup', data).then(response => {
        // console.log('SIGNUP response', data.token)
        if (response && response.data && response.data.token) {
          commit('SET_TOKEN', response.data.token)
          commit('SET_LEVEL', response.data.level)
        }
        resolve(response.data)
      }).catch(err => {
        // console.log('LOGIN error', data)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  REGISTER_AGENCY ({commit, dispatch}, data) {
    // console.log('REGISTER_AGENCY action', data)
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.post('/api/v1/agent', data).then(response => {
        // console.log('REGISTER_AGENCY response', data.token)
        resolve(response.data)
      }).catch(err => {
        // console.log('LOGIN error', data)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
