import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import news from './modules/news'
import settings from './modules/settings'

Vue.use(Vuex)

// const LocalStorage = (store) => {
//   store.subscribe((mutation) => {
//     window.localStorage.setItem('auth', JSON.stringify(user.state))
//     if (mutation.type === 'CLEAR_ALL') {
//       window.localStorage.removeItem('auth')
//     }
//   })
// }

export default new Vuex.Store({
  modules: {
    app,
    user,
    news,
    settings
  }
  // plugins: [LocalStorage]
})
