const state = {
  subscriptions: [],
  newsItems: [],
  fontSizes: ['small', 'medium', 'large'],
  cardLayouts: ['grid', 'list', 'cards'],
  selectedFontSize: 'medium',
  selectedCardLayout: 'grid'
}
const getters = {
  allSubscriptions: (state) => state.subscriptions,
  fontSizes: (state) => state.fontSizes,
  cardLayouts: (state) => state.cardLayouts,
  selectedFontSize: (state) => state.selectedFontSize,
  selectedCardLayout: (state) => state.selectedCardLayout
}
const mutations = {
  SET_SUBSCRIPTIONS (state, payload) {
    state.subscriptions = payload
  },
  CHANGE_LAYOUT (state, payload) {
    if (state.cardLayouts.indexOf(payload) > -1) {
      state.selectedCardLayout = payload
    }
  },
  CHANGE_FONT_SIZE (state, payload) {
    if (state.fontSizes.indexOf(payload) > -1) {
      state.selectedFontSize = payload
    }
  }
}
const actions = {
  GET_SUBSCRIPTIONS ({commit, dispatch}, payload) {
    // console.log('GET_NEWS_CATEGORIES action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/subscriptions').then(response => {
        // console.log('GET_NEWS_CATEGORIES response', response)
        if (response && response.data && response.data.categories) {
          commit('SET_SUBSCRIPTIONS', response.data.categories)
        }
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        // console.log('GET_NEWS_CATEGORIES error', err)
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  SEARCH_SUBSCRIPTION_CHANNELS ({commit, dispatch}, payload) {
    // console.log('GET_NEWS_CATEGORIES action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/search/channel?q=' + payload.text + '&category=' + payload.category).then(response => {
        commit('NO_NETWORK', false)
        resolve(response.data)
      }).catch(err => {
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  },
  NOTIFICATION_SUBSCRIPTION ({state, commit, dispatch}, payload) {
    console.log('NOTIFICATION_SUBSCRIPTION action')
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.post('/api/v1/subscriptions', payload).then(response => {
        commit('NO_NETWORK', false)
        resolve(response.data)
        if (state.subscriptions.filter(item => item.name).length === 0 && payload && payload.hasOwnProperty('name') && payload.hasOwnProperty('value')) {
          state.subscriptions.push(payload)
          localStorage.setItem('subscriptions', JSON.stringify(state.subscriptions))
        }
      }).catch(err => {
        dispatch('HANDLE_ERROR', err)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
